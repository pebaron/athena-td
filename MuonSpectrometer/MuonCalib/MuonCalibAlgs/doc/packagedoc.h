/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page MuonCalibAlgs_page MuonCalibAlgs Package
@author Niels.Van.Eldik@cern.ch

@section MuonCalibAlgs_MuonCalibAlgsIntro Introduction
This package contains the ATHENA algorithms that are common for all muon 
technologies.

@section MuonCalibAlgs_MuonCalibAlgsOverview Class Overview
The MuonCalibAlgs package contains the following class

- MuonCalib::MuonCalibAlg: ATHENA Algorithm retrieving the 'ingredients' to construct a MuonCalibEvent from storegate and passing them to a calibration tool. See the MuonCalibEvent class description for the content of a MuonCalibEvent.



*/
